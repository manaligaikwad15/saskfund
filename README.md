### Architecture

#### 1. UI in Angular (sask-fund-app)
#### 2. API in nodejs (rest-api)
#### 3. Database in MySQL (saskfunds_create_tables.sql)


#### Setup steps

1. Run `saskfunds_create_tables.sql` to create tables
2. Run `cd sask-fund-app && ng serv` to run the angular UI
2. Run `cd rest-api && server.js` to run nodejs API

#### Eligibility Query

```
SELECT *,
       (age > 25 OR (age > 21 AND education > 3))
           AND house_value = 0
           AND length_at_residence > 5
           AND
       (((credit_card_debt * 0.18) + (charitable_donations / 12) + (1100 * (monthly_living_cost_factor / 100))) <
        (income_factor / 100) * (income / 12)) is_eligeible_for_loan
FROM (
         SELECT id,
                name,
                gender,
                age,
                city,
                gold_membership,
                income,
                education,
                length_at_residence,
                charitable_donations,
                credit_card_debt,
                house_value,
                CASE
                    WHEN city = 'Regina' THEN 96.95
                    WHEN city = 'Saskatoon' THEN 99.8
                    WHEN city = 'Moose Jaw' THEN 103.25
                    WHEN city = 'North Battleford' THEN 105.1
                    ELSE 100
                    END as monthly_living_cost_factor,
                CASE
                    WHEN gold_membership = true THEN 45
                    ELSE 40
                    END as income_factor
         FROM clients_personal
                  JOIN clients_financial ON clients_personal.id = clients_financial.client_id) as summary
WHERE id = 9;
```

#### Caching

I've implemented caching using a table `clients_data_cache` where I store `client_id` and `is_eligible` along with timestamps.

The logic for caching is implemented in `eligibility_validation.js` file.


