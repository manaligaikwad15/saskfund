CREATE TABLE clients_personal (
    id int NOT NULL,
    name varchar(255) NOT NULL,
    gender varchar(255),
    age int,
    city varchar(255),
    length_at_residence int,
    education int,
    PRIMARY KEY (id)
);

CREATE TABLE clients_financial (
    client_id int,
    income int,
    house_value int,
    employment_years int,
    credit_card_debt int,
    charitable_donations int,
    gold_membership boolean,
    FOREIGN KEY (client_id) REFERENCES clients_personal(id)
);

CREATE TABLE clients_data_cache (
    client_id int NOT NULL,
    is_eligible boolean NOT NULL,
    createdAt datetime,
    updatedAt datetime,
    PRIMARY KEY (client_id)
);
