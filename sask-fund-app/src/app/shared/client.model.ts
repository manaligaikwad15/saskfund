export class ClientModel{
  public client_id: number;
  public name: string;
  public gender: string;
  public age: number;
  public city: string;
  public gold_membership_status: boolean;
  public is_eligibility: boolean;
  public maximum_monthly_payment: number;


  constructor(client_id: number, name: string, gender: string, age: number, city: string, golden_Membership_status: boolean, eligible_for_loan: boolean, max_monthaly_payment: number) {
    this.client_id = client_id;
    this.name = name;
    this.gender = gender;
    this.age = age;
    this.city = city;
    this.gold_membership_status = golden_Membership_status;
    this.is_eligibility = eligible_for_loan;
    this.maximum_monthly_payment = max_monthaly_payment;
  }
}
