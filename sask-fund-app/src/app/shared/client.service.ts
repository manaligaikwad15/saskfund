import {ClientModel} from './client.model';
import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class ClientService{
  clientSelected = new EventEmitter<ClientModel>();
  public matchingClientsFound = new Subject<ClientModel[]>();

  private url = 'http://localhost:3001/api/clients/';

  constructor(private http: HttpClient) {
  }

  // getClients(){
  //   return this.clientsFound.slice();
  // }

  getClientByClientId(client_id: number){
    return this.http.get(this.url+client_id);
  }

  getClientsByName(name: string) {
    return this.http.get(this.url+"all/"+name);
  }


}
