import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SearchClientsComponent } from './search-clients/search-clients.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ClientService} from './shared/client.service';
import { ClientItemComponent } from './clients-list/client-item/client-item.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchClientsComponent,
    ClientsListComponent,
    ClientDetailsComponent,
    ClientItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
