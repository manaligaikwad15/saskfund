import {Component, OnInit} from '@angular/core';
import {ClientModel} from './shared/client.model';
import {ClientService} from './shared/client.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'sask-fund-app';

  selectedClient: ClientModel;

  constructor(private clientService: ClientService) { }

  ngOnInit() {
    this.clientService.clientSelected
      .subscribe(
        (client: ClientModel) => {
          this.selectedClient = client;
        }
      );
  }
}
