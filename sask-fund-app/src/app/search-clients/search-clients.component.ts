import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ClientService} from '../shared/client.service';

@Component({
  selector: 'app-search-clients',
  templateUrl: './search-clients.component.html',
  styleUrls: ['./search-clients.component.css']
})
export class SearchClientsComponent implements OnInit {
  private clients = [];
  private clientById;
  constructor(private clientService: ClientService) { }

  ngOnInit(): void {
  }

  searchClients(form: NgForm) {
    let clientIdInput = form.controls['clientID'].value;
    let nameInput = form.controls['name'].value;
    this.clients = []

    if (nameInput !== "") {
      this.clientService.getClientsByName(nameInput)
        .toPromise()
        .then((response: Response) => {
          console.log(response);
            this.clients = this.clients.concat(response);

            this.clientService.matchingClientsFound.next(this.clients);
        }
        )
        .catch(this.handleError);
    } else{
      this.clientService.getClientByClientId(clientIdInput)
        .toPromise()
        .then((response: Response) => {
            this.clientById = response;

            this.clients.push(this.clientById);
          this.clientService.matchingClientsFound.next(this.clients);
          }
        )
        .catch(this.handleError);
    }


  }

  private handleError (error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }


}
