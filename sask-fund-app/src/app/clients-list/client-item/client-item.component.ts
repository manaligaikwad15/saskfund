import {Component, Input, OnInit} from '@angular/core';
import {ClientService} from '../../shared/client.service';
import {ClientModel} from '../../shared/client.model';

@Component({
  selector: 'app-client-item',
  templateUrl: './client-item.component.html',
  styleUrls: ['./client-item.component.css']
})
export class ClientItemComponent implements OnInit {
  @Input() client: ClientModel;
  constructor(private clientService: ClientService) { }

  ngOnInit(): void {
  }

  onSelected() {
    this.clientService.clientSelected.emit(this.client);
  }

  getLoanEligibility(){
    return this.client.is_eligibility;
  }

  getColour() {
    return this.client.is_eligibility ? 'green':'red';
  }
}
