import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {ClientModel} from '../shared/client.model';
import {ClientService} from '../shared/client.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})
export class ClientsListComponent implements OnInit {
  clientsFound: ClientModel[];

  constructor(private clientService: ClientService) {
    this.clientService.matchingClientsFound.subscribe(
      (clients: ClientModel[]) => {
        this.clientsFound = []; // clear previous results
        this.clientsFound = clients;
        // console.log(this.matchingImages);
        console.log(this.clientsFound.length);
        // }
      }
    );
  }

  ngOnInit(): void {
    // this.clientsFound = this.clientService.getClients();

  }



}
