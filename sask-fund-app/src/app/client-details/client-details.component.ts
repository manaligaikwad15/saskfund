import {Component, Input, OnInit} from '@angular/core';
import {ClientService} from '../shared/client.service';
import {ClientModel} from '../shared/client.model';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {
  @Input() client: ClientModel;
  constructor() {}

  ngOnInit(): void {
  }

  getColour() {
    return this.client.is_eligibility ? 'green':'red';
  }
}
