module.exports = app => {
    const clients = require("../controllers/client.controller.js");

    var router = require("express").Router();

    // Retrieve all Clients by name
    router.get("/all/:name", clients.findAll);

    // Retrieve a single client with an id
    router.get("/:id", clients.findOne);

    app.use('/api/clients', router);
};
