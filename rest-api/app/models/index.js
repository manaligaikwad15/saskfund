const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.clients = require("./client_personal_profiles.js")(sequelize, Sequelize);
db.clients_financial = require("./client_financial_profiles.js")(sequelize, Sequelize);
db.clients_data_cache = require("./client_data_cache.js")(sequelize, Sequelize);

db.clients.hasOne(db.clients_financial, {foreignKey: 'client_id'});
// db.client_financial_profiles.belongsTo(db.clients, {targetKey: 'client_id'});

module.exports = db;
