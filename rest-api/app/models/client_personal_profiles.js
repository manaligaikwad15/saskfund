module.exports = (sequelize, DataTypes) => {
    const clients_personal = sequelize.define("clients_personal", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true
            },
            name: {
                type: DataTypes.STRING
            },
            gender: {
                type: DataTypes.STRING
            },
            age: {
                type: DataTypes.STRING
            },
            city: {
                type: DataTypes.STRING
            },
            length_at_residence: {
                type: DataTypes.INTEGER,
            },
            education: {
                type: DataTypes.INTEGER,
            }
        }, {
            freezeTableName: true,
            timestamps: false
        }
    );

    return clients_personal;
};
