module.exports = (sequelize, DataTypes) => {
    const clients_data_cache = sequelize.define("clients_data_cache", {
            client_id: {
                type: DataTypes.INTEGER,
                primaryKey: true
            },
            is_eligible: {
                type: DataTypes.BOOLEAN
            }
        }, {
            freezeTableName: true,
            timestamps: true
        }
    );

    return clients_data_cache;
};
