module.exports = (sequelize, DataTypes) => {
    const Client = sequelize.define("clients_financial", {
            client_id: {
                type: DataTypes.INTEGER,
                primaryKey: true
            },
            income: {
                type: DataTypes.INTEGER
            },
            house_value: {
                type: DataTypes.INTEGER
            },
            employment_years: {
                type: DataTypes.INTEGER
            },
            credit_card_debt: {
                type: DataTypes.INTEGER
            },
            charitable_donations: {
                type: DataTypes.INTEGER
            },
            gold_membership: {
                type: DataTypes.BOOLEAN
            }
        }, {
            freezeTableName: true,
            timestamps: false
        }
    );
    return Client;
};
