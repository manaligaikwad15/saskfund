module.exports = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "root@1234",
    DB: "sask_fund",
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};
