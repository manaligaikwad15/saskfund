const db = require("../models");
const eligibilityValidationSvc = require("../services/eligibility_validation.js")
const Client = db.clients;
const Op = db.Sequelize.Op;

// Find a all clients with name
exports.findAll = (req, res) => {
    const name = req.params.name;

    var condition = name ? {name: {[Op.like]: `%${name}%`}} : null;

    Client.findAll({where: condition})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving clients."
            });
        });
};

// Find a all clients with an ID
exports.findOne = (req, res) => {
    const id = req.params.id;
    eligibilityValidationSvc.checkIsEligibilityForLoan(id).then(
        data => {
            res.send(data);
        }
    )
};
