const city = {
    REGINA: 'Regina',
    SASKATOON: 'Saskatoon',
    MOOSE_JAW: 'Moose Jaw',
    NORTH_BATTLEFORD: 'North Battleford'
}

module.exports.getMonthlyLivingCostByCity = function (city_name) {
    const base_monthly_living_cost_in_sk = 1100;
    let cost, factor;
    console.log('City: [%s]', city_name);

    switch(city_name){
        case city.REGINA:
            factor = 0.9695;
            break;
        case city.SASKATOON:
            factor = 0.998;
            break;
        case city.MOOSE_JAW:
            factor = 1.0325;
            break;
        case city.NORTH_BATTLEFORD:
            factor = 1.051;
            break;
        default:
            factor = 1
    }

    cost = base_monthly_living_cost_in_sk * factor;

    console.log('cost factor: [%d]', factor);
    return cost;
}
