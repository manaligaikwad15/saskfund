const utility = require("./utility_svc.js");
const db = require("../models");
const clients_data_cache = db.clients_data_cache;
const client = db.clients;
const moment = require('moment');
const sequelize = db.sequelize
const { Op } = require('sequelize')

module.exports.checkIsEligibilityForLoan = async function (id) {
    let valid, client_data, is_eligible_from_db = false, valid_cache, is_cached

    valid_cache_array = await clients_data_cache.findAll({
        where: {
            client_id: id,
            updatedAt: {
                [Op.gte]: moment().subtract(1, 'minutes').toDate()
            }
        }
    })

    valid_cache = valid_cache_array[0]

    if(!valid_cache) {
        client_data_array = await db.sequelize.query('SELECT *, (age > 25 OR (age > 21 AND education > 3)) AND house_value = 0 AND length_at_residence > 5 AND (((credit_card_debt * 0.18) + (charitable_donations / 12) + (1100 * (monthly_living_cost_factor / 100))) < (income_factor / 100) * (income / 12)) is_eligibility FROM ( SELECT id , name, gender, age, city, gold_membership, income, education, length_at_residence, charitable_donations, credit_card_debt, house_value, CASE WHEN city = \'Regina\' THEN 96.95 WHEN city = \'Saskatoon\' THEN 99.8 WHEN city = \'Moose Jaw\' THEN 103.25 WHEN city = \'North Battleford\' THEN 105.1 ELSE 100 END as monthly_living_cost_factor, CASE WHEN gold_membership = true THEN 45 ELSE 40 END as income_factor FROM clients_personal JOIN clients_financial ON clients_personal.id = clients_financial.client_id) as summary where id = :status',
            {replacements: {status: id}, type: sequelize.QueryTypes.SELECT}
        );
        client_data = client_data_array[0]
        is_eligible_from_db = client_data.is_eligibility

        if(client_data) {
            await clients_data_cache.upsert({client_id: id, is_eligible: is_eligible_from_db})
        }

        is_cached = false
    }else{
        client_data = await client.findByPk(id)

        is_eligible_from_db = valid_cache.is_eligible

        is_cached = true
    }

    valid = {
        'client_id': client_data.id,
        'name': client_data.name,
        'gender': client_data.gender,
        'age': client_data.age,
        'city': client_data.city,
        'is_eligibility': Boolean(is_eligible_from_db),
        'is_cached': is_cached
    }

    return valid;
};


